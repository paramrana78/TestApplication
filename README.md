# TestApplication

Screen:1

 -Just normal login screen with username and password.
 
![Screen_1](https://user-images.githubusercontent.com/17310188/132246048-b01ab0ad-4211-4bc2-a877-8e63153bd044.png)


Screen: 2

 -Login button disabled until both username and password filled properly.
 Now properly means, length of both fields should be greater than 5.

![Screen_2](https://user-images.githubusercontent.com/17310188/132246051-0c162276-46e1-407f-bbef-2663baeed936.png)

Screen: 3

 -Dashboard with a hello message in the centre.

![Screen_3](https://user-images.githubusercontent.com/17310188/132246060-f766bb0b-6c7e-4045-a83c-1d4801713561.png)
